import {Component, OnInit} from '@angular/core';

import {HsConfig, HsLayoutService} from 'hslayers-ng';

import {AppConfig, AppConfigService} from 'atlas-components';

import {environment} from '../environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  appConfig: HsConfig;

  constructor(
    private appConfigService: AppConfigService,
    public HsConfig: HsConfig,
    public HsLayoutService: HsLayoutService
  ) {}
  title = 'regionalni-speciality-atlas';

  ngOnInit(): void {
    this.appConfig = this.appConfigService.config;

    if (this.appConfig) {
      this.HsConfig.proxyPrefix = environment.proxyPrefix || '/proxy/';
      Object.assign(this.HsConfig, this.appConfig);
    }
    this.HsLayoutService.setDefaultPanel('atlas_features');
  }
}
