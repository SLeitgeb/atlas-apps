import { TestBed } from '@angular/core/testing';

import { AtlasComponentsService } from './atlas-components.service';

describe('AtlasComponentsService', () => {
  let service: AtlasComponentsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AtlasComponentsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
