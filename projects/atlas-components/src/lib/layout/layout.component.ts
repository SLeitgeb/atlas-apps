import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ElementRef,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';

import {Select} from 'ol/interaction';
import View from 'ol/View';
import {HsConfig, HsEventBusService, HsLayoutService, HsQueryBaseService, HsMapService} from 'hslayers-ng';
import {HsMatLayoutComponent} from 'hslayers-material';

import {HsMapHostDirective} from './map-host.directive';

@Component({
  selector: 'hs-atlas-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class LayoutComponent extends HsMatLayoutComponent {
  @ViewChild(HsMapHostDirective, {static: true}) mapHost: HsMapHostDirective;
  selector: Select;
  lastView: any;

  constructor(
    public HsConfig: HsConfig,
    public HsLayoutService: HsLayoutService,
    public HsEventBusService: HsEventBusService,
    public HsQueryBaseService: HsQueryBaseService,
    // public HsThemeService: HsThemeService,
    elementRef: ElementRef,
    cdr: ChangeDetectorRef, // private HsUtilsService: HsUtilsService
    public HsMapService: HsMapService
  ) {
    super(
      HsConfig,
      HsLayoutService,
      HsEventBusService,
      elementRef,
      cdr
    );
    
    this.HsQueryBaseService.vectorSelectorCreated.subscribe(selector => {
      this.selector = selector;
    });

    this.HsEventBusService.vectorQueryFeatureSelection.subscribe(e => {
      const currentView = this.HsMapService.map.getView();
      // const currentView = this.HsMapService.map.getView().getProperties();
      const currentZoom = currentView.getZoom();
      const maxZoom = currentView.getMaxZoom();
      // const zoomStep = this.HsConfig?.selectionZoomStep || 2;
      const zoomStep = 2;
      const zoomAvailable = maxZoom - currentZoom;
      const newZoom =
        currentZoom +
        zoomStep * Number(zoomAvailable >= zoomStep) +
        zoomAvailable * Number(zoomAvailable < zoomStep);

      this.lastView = currentView.getState();
      // this.lastView = currentView;

      this.HsMapService.map.getView().cancelAnimations();
      this.HsMapService.map.getView().animate({
        zoom: newZoom,
        center: e.feature.getProperties().geometry.flatCoordinates,
        duration: 300,
      });
    });

    this.HsEventBusService.vectorQueryFeatureDeselection.subscribe(e => {
      this.HsMapService.map.getView().animate({
        zoom: this.lastView.zoom,
        center: this.lastView.center,
        duration: 300,
      });
    });
  }
}
