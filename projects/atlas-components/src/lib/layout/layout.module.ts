import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {HsLayoutService, HsMapModule} from 'hslayers-ng';
import {
  HsMatLayoutModule,
} from 'hslayers-material';
import {LayoutComponent} from './layout.component';
import {FeaturesModule} from '../features/features.module';
import {HsMapHostDirective} from './map-host.directive';

@NgModule({
  declarations: [
    HsMapHostDirective,
    LayoutComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  imports: [
    CommonModule,
    HsMapModule,
    FeaturesModule,
    HsMatLayoutModule,
  ],
  providers: [
    HsLayoutService,
  ],
  entryComponents: [LayoutComponent],
  exports: [LayoutComponent],
})
export class LayoutModule {}
