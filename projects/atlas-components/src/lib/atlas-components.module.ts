import {NgModule} from '@angular/core';

import {HsCoreModule} from 'hslayers-ng';

import {FeaturesModule} from './features/features.module';
import {LayoutModule} from './layout/layout.module';
import {AtlasComponent} from './atlas-components.component';
import {AtlasMaterialModule} from './material-module';

@NgModule({
  declarations: [AtlasComponent],
  imports: [HsCoreModule, AtlasMaterialModule, LayoutModule, FeaturesModule],
  exports: [AtlasComponent],
})
export class AtlasComponentsModule {}
