import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import {Vector as VectorLayer, Tile as TileLayer} from 'ol/layer';
import {transform} from 'ol/proj';
import {XYZ, Vector as VectorSource} from 'ol/source';
import {GeoJSON} from 'ol/format';
import View from 'ol/View';

import {HsConfig} from 'hslayers-ng';

@Injectable({
  providedIn: 'root'
})
export class AppConfigService {

  public config: HsConfig;

  constructor(private http: HttpClient) { }

  load() {
    return this.http.get('assets/config.json')
      .toPromise()
      .then(data => {
        this.config = this.parseConfig(data);
      });
  }

  parseConfig(config: any): HsConfig {
    let parsed: HsConfig = {};
    parsed.default_layers = [
      ...config.default_layers.XYZ.map(item => new TileLayer({
        ...item,
        source: new XYZ(item.source)
      })),
      ...config.default_layers.GeoJSON.map(item => new VectorLayer({
        ...item,
        source: new VectorSource({
          ...item.source,
          format: new GeoJSON()
        })
      }))
    ];
    parsed.default_view = new View({
      zoom: 4,
      units: 'm',
      maxZoom: 9,
      minZoom: 2,
      constrainResolution: true,
      ...config.default_view,
      center: transform(config.default_view?.center || [8.3927408, 46.9205358], 'EPSG:4326', 'EPSG:3857'),
    });
    parsed.panelWidths = config?.panelWidths;
    parsed.queryPoint = config?.queryPoint || 'hidden';
    parsed.layersInFeatureTable = parsed.default_layers.filter(layer => layer.get('title') === 'Atlas features');

    return parsed;
  }
}
