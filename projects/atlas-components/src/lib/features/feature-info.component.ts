import {
  Component,
  ChangeDetectorRef,
  ElementRef,
  Input,
  AfterViewInit,
  ViewChild
} from '@angular/core';

import ResizeObserver from 'resize-observer-polyfill';
import Feature from 'ol/Feature';
import {HsConfig} from 'hslayers-ng';
import {
  HsMatSlideshowService,
  HsMatSlideshowRef
} from 'hslayers-material';

@Component({
  selector: 'hs-atlas-feature-info',
  templateUrl: './feature-info.html',
  styleUrls: ['feature-info.scss'],
})
export class AtlasFeatureInfoComponent implements AfterViewInit {
  @ViewChild('gallery') gallery: ElementRef;
  @Input()
  get feature(): any { return this._feature; }
  set feature(feature: Feature<any>) {
    this._feature = feature.getProperties();
    this._feature.gallery = this.getImages(this._feature?.gallery);
  }
  private _feature: any = {};

  constructor(
    private cd: ChangeDetectorRef,
    private slideshow: HsMatSlideshowService,
  ) {}

  getImages(gallery): {} {
    if (!gallery) return undefined;
    return gallery.filter(i => i.type === 'image');
  }

  findRow(gallery, targetRatio, row=[], rowRatio=0) {
    if (gallery.length === 0) return [row, gallery, rowRatio];

    const image = gallery[0];
    const imageRatio = image.width / image.height;
    const imageFits = targetRatio - rowRatio > imageRatio / 2 || row.length === 0;
    
    if (!imageFits) return [row, gallery, rowRatio];

    const ratio = rowRatio + imageRatio;
    row.push(image);
    const remainingGallery = gallery.slice(1);

    return this.findRow(remainingGallery, targetRatio, row, ratio);
  }

  setSizes(row, ratio, targetRatio, isLastRow, galleryWidth) {
    const gap = Number(getComputedStyle(this.gallery.nativeElement).gap.replace('px', ''));

    // TODO: consider a more robust calculation of rowRatio
    // gaps are not currently considered in the rowRatio
    // if (isLastRow && ((targetRatio - ratio) / targetRatio > targetRatio / 10)) ratio = targetRatio;
    let availableWidth = galleryWidth - gap * (row.length - 1);
    // const rowHeight = availableWidth / ratio;

    row.forEach((image, i) => {
      const imageRatio = image.width / image.height;
      // image.c_height = rowHeight + 'px';
      // image.c_width = rowHeight * imageRatio + 'px';

      // image.c_height = 'auto';

      if (isLastRow && ((targetRatio - ratio) / targetRatio > targetRatio / 10)) {
        ratio = targetRatio;
        const rowHeight = availableWidth / ratio;
        image.c_width = rowHeight * imageRatio + 'px';
        return;
      }

      const newValue = row.length > 1 ? `calc((100% - ${gap * (row.length - 1)}px) * ${imageRatio / ratio})` : '100%';
      if (image.c_width !== newValue) image.c_width = newValue;
    });
  }

  findRows(gallery, targetRatios, galleryWidth) {
    const targetRatio = targetRatios[0];

    const [row, remainingImages, rowRatio] = this.findRow(gallery, targetRatio);
    const isLastRow = remainingImages.length === 0;
    this.setSizes(row, rowRatio, targetRatio, isLastRow, galleryWidth);

    const remainingRatios = targetRatios.length > 1 ? targetRatios.slice(1) : targetRatios;
    if (!isLastRow) this.findRows(remainingImages, remainingRatios, galleryWidth);
  }

  calculateLayout(feature, galleryWidth): void {
    const targetRatios = [2, 4];
    this.findRows(feature.gallery, targetRatios, galleryWidth);
    this.cd.detectChanges();
  }

  openSlideshow(): void {
    let dialogRef: HsMatSlideshowRef = this.slideshow.open({
      gallery: this._feature.gallery
    });
  }

  ngAfterViewInit(): void {
    let oldGalleryWidth = 0;
    const ro = new ResizeObserver((entries, observer) => {
      for (const entry of entries) {
        const {left, top, width, height} = entry.contentRect;
        if (width > 0 && oldGalleryWidth !== width) this.calculateLayout(this._feature, width);
        oldGalleryWidth = width;
      }
    });

    ro.observe(this.gallery.nativeElement);
  }
}
