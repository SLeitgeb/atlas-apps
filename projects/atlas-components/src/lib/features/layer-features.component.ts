import {Component, Input} from '@angular/core';
import {Select} from 'ol/interaction';
import {
  HsLayerFeaturesComponent,
  HsFeatureTableService,
  HsUtilsService,
  HsMapService,
  HsLanguageService,
  HsLayerUtilsService,
  HsQueryBaseService,
} from 'hslayers-ng';

@Component({
  selector: 'hs-atlas-layer-features',
  templateUrl: 'layer-features.html',
  styleUrls: ['layer-features.scss'],
})
export class AtlasLayerFeaturesComponent extends HsLayerFeaturesComponent {
  @Input('layer') layer: any;
  selector: Select;

  constructor(
    public HsFeatureTableService: HsFeatureTableService,
    public HsUtilsService: HsUtilsService,
    public HsMapService: HsMapService,
    public HsLanguageService: HsLanguageService,
    public HsLayerUtilsService: HsLayerUtilsService, //Used in template
    public HsQueryBaseService: HsQueryBaseService
  ) {
    super(
      HsFeatureTableService,
      HsUtilsService,
      HsMapService,
      HsLanguageService,
      HsLayerUtilsService,
    );

    this.selector = this.HsQueryBaseService.selector;
  }

  selectFeature(feature: any): void {
    this.selector.getFeatures().push(feature);
  }
}
