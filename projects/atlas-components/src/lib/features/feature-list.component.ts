import {Component} from '@angular/core';
import {HsFeatureTableComponent} from 'hslayers-ng';

@Component({
  selector: 'hs-atlas-feature-list',
  templateUrl: './feature-list.html',
})
export class AtlasFeatureListComponent extends HsFeatureTableComponent {}
