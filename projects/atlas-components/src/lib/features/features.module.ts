import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {HsFeatureTableService} from 'hslayers-ng';
import {
  HsMatSlideshowModule,
  HsMatSlideshowService
} from 'hslayers-material';
import {AtlasFeatureInfoComponent} from './feature-info.component';
import {AtlasFeatureListComponent} from './feature-list.component';
import {AtlasLayerFeaturesComponent} from './layer-features.component';
import {AtlasMaterialModule} from '../material-module';

@NgModule({
  declarations: [
    AtlasFeatureInfoComponent,
    AtlasFeatureListComponent,
    AtlasLayerFeaturesComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  imports: [
    CommonModule,
    AtlasMaterialModule,
  ],
  providers: [
    HsFeatureTableService,
    HsMatSlideshowService
  ],
  bootstrap: [AtlasFeatureListComponent, AtlasFeatureInfoComponent, AtlasLayerFeaturesComponent],
  entryComponents: [AtlasFeatureInfoComponent, AtlasFeatureListComponent],
  exports: [AtlasFeatureInfoComponent, AtlasFeatureListComponent, AtlasLayerFeaturesComponent],
})
export class FeaturesModule {}
