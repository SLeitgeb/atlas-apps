import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AtlasComponentsComponent } from './atlas-components.component';

describe('AtlasComponentsComponent', () => {
  let component: AtlasComponentsComponent;
  let fixture: ComponentFixture<AtlasComponentsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AtlasComponentsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AtlasComponentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
