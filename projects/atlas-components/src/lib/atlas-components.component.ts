import {Component, Input, OnInit} from '@angular/core';

import {HsConfig} from 'hslayers-ng';

@Component({
  selector: 'hs-atlas',
  template: `
    <hs-atlas-layout
      #hslayout
      style="height: 100%; max-height: 100vh; position: relative; width: 100%; display: block"
      class="hs-atlas-layout"
      layout="column"
    ></hs-atlas-layout>
  `,
  styles: [],
})
export class AtlasComponent implements OnInit {
  @Input() config: HsConfig;
  constructor(public HsConfig: HsConfig) {}

  ngOnInit(): void {
    if (this.config) {
      this.HsConfig.update(this.config);
    }
  }
}
